package com.libreria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.libreria.bean.AutorBean;
import com.libreria.service.AutorService;

/**
 * @author gonzalojavierdiaz
 *
 */
@RestController
@RequestMapping("/autor")
public class AutorController {
	
	@Autowired
	private AutorService autorService;
	
	@RequestMapping("/alta")
	public void altaAutor(@RequestBody AutorBean autorBean){
		autorService.altaAutor(autorBean);
	}

	@RequestMapping("/baja/{idAutor}")
	public void bajaAutor(@PathVariable Long idAutor){
		autorService.bajaAutor(idAutor);
	}

	@RequestMapping("/modificacion")
	public void modificacionAutor(@RequestBody AutorBean autorBean){
		autorService.modificarAutor(autorBean);
	}

	@RequestMapping("/autores")
	public List<AutorBean> autores(){
		return autorService.getAutores();
	}

	@RequestMapping("/{idAutor}")
	public AutorBean autor(@PathVariable Long idAutor){
		return autorService.getAutorById(idAutor);
	}

}
