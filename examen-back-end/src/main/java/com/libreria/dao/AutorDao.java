package com.libreria.dao;

import com.libreria.bean.AutorBean;
import com.libreria.commons.dao.GenericJpaDao;
import com.libreria.entity.Autor;

/**
 * @author gonzalojavierdiaz
 *
 */
public interface AutorDao extends GenericJpaDao<Autor, Long> {

	Autor getAutor(AutorBean autor);
	
	Autor getByDocument(String tipo, Long numDoc);
}
