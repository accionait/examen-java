package com.libreria.dao.impl;

import org.springframework.stereotype.Repository;

import com.libreria.bean.AutorBean;
import com.libreria.commons.dao.impl.GenericJpaDaoImpl;
import com.libreria.dao.AutorDao;
import com.libreria.entity.Autor;

/**
 * @author gonzalojavierdiaz
 *
 */
@Repository
public class AutorDaoImpl extends GenericJpaDaoImpl<Autor, Long> implements AutorDao {

	private static final long serialVersionUID = -6896505434357441934L;

	/* (non-Javadoc)
	 * @see com.libreria.dao.AutorDao#getAutor(com.libreria.bean.AutorBean)
	 */
	@Override
	public Autor getAutor(AutorBean user) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.libreria.dao.AutorDao#getByDocument(java.lang.String, java.lang.Long)
	 */
	@Override
	public Autor getByDocument(String tipo, Long numDoc) {
		// TODO Auto-generated method stub
		return null;
	}

}
