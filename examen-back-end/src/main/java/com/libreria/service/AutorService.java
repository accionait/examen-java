package com.libreria.service;

import java.util.List;

import com.libreria.bean.AutorBean;

/**
 * @author gonzalojavierdiaz
 *
 */
public interface AutorService {

	AutorBean getAutor(AutorBean autorBean);
	
	AutorBean getByDocument(String tipo, Long numDoc);
	
	void altaAutor(AutorBean autorBean);
	
	void bajaAutor(Long idAutor);
	
	void modificarAutor(AutorBean autorBean);
	
	List<AutorBean> getAutores();
	
	AutorBean getAutorById(Long idAutor);
	

}
