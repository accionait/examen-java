package com.libreria.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.libreria.bean.AutorBean;
import com.libreria.dao.AutorDao;
import com.libreria.entity.Autor;
import com.libreria.service.AutorService;

/**
 * @author gonzalojavierdiaz
 *
 */
@Service
public class AutorServiceImpl implements AutorService {
	
	@Autowired
	private AutorDao autorDao;

	/*
	 * (non-Javadoc)
	 * @see com.libreria.service.AutorService#getAutor(com.libreria.bean.AutorBean)
	 */
	@Override
	public AutorBean getAutor(AutorBean autor) {
		AutorBean autorBean = new AutorBean();
		BeanUtils.copyProperties(autorDao.getAutor(autor), autorBean);
		return autorBean;
	}

	/* (non-Javadoc)
	 * @see com.libreria.service.AutorService#getByDocument(java.lang.String, java.lang.Long)
	 */
	@Override
	public AutorBean getByDocument(String tipo, Long numDoc) {
		AutorBean autorBean = new AutorBean();
		BeanUtils.copyProperties(autorDao.getByDocument(tipo, numDoc), autorBean);
		return autorBean;
	}

	/*
	 * (non-Javadoc)
	 * @see com.libreria.service.AutorService#altaAutor(com.libreria.bean.AutorBean)
	 */
	@Override
	@Transactional
	public void altaAutor(AutorBean autorBean) {
		Autor autor = new Autor();
		BeanUtils.copyProperties(autorBean, autor);
		autorDao.saveOrUpdate(autor);
	}

	/*
	 * (non-Javadoc)
	 * @see com.libreria.service.AutorService#bajaAutor(com.libreria.bean.AutorBean)
	 */
	@Override
	@Transactional
	public void bajaAutor(Long idAutor) {
		autorDao.delete(autorDao.findById(idAutor));
	}

	/*
	 * (non-Javadoc)
	 * @see com.libreria.service.AutorService#modificarAutor(com.libreria.bean.AutorBean)
	 */
	@Override
	@Transactional
	public void modificarAutor(AutorBean autorBean) {
		Autor autor = autorDao.findById(autorBean.getIdAutor());
		BeanUtils.copyProperties(autorBean, autor);
		autorDao.saveOrUpdate(autor);
	}

	/*
	 * (non-Javadoc)
	 * @see com.libreria.service.AutorService#getAutores()
	 */
	@Override
	public List<AutorBean> getAutores() {
		List<AutorBean> autoresBean = new ArrayList<AutorBean>();
		List<Autor> autores = autorDao.findAll();
		AutorBean autorBean;
		for(Autor a : autores){
			autorBean = new AutorBean();
			BeanUtils.copyProperties(a, autorBean);
			autoresBean.add(autorBean);
		}
		return autoresBean;
	}

	/*
	 * (non-Javadoc)
	 * @see com.libreria.service.AutorService#getAutorById(java.lang.Long)
	 */
	@Override
	public AutorBean getAutorById(Long idAutor) {
		AutorBean autorBean = new AutorBean();
		BeanUtils.copyProperties(autorDao.findById(idAutor), autorBean);
		return autorBean;
	}

}
