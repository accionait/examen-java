function crearAutor($scope, $http, $window) {
	$scope.fields = null;
    $scope.formAltaAutor = function() {
  	  var data = $scope.fields;
  	  $http({
  		  method: 'POST',
  		  url: 'http://localhost:8080/autor/alta',
  		  data: JSON.stringify(data),
  		  headers: {'Content-Type': 'application/json'}
  	  }).then(function responseOK(response){
  		  $window.location.href = '../listAutores.html';
  		  });
    };
}
function modificarAutor($scope, $http, $window) {
    $scope.formModificarAutor = function() {
  	  var data = $scope.fields;
  	  $http({
  		  method: 'POST',
  		  url: 'http://localhost:8080/autor/modificacion',
  		  data: JSON.stringify(data),
  		  headers: {'Content-Type': 'application/json'}
  	  }).then(function responseOK(response){
  		  $window.location.href = '../listAutores.html';
  	  	});
    };
	$http.get('http://localhost:8080/autor/autores').success(
			function(data){$scope.autores = data;}
	);
	$scope.autorChanged = function(idAutor){
		getAutor($scope, $http, idAutor)
	};
}
function getAutor($scope, $http, idAutor){
	$http.get('http://localhost:8080/autor/'+idAutor).success(
			function(data){$scope.fields = data;}
	);
}
function getAutores($scope, $http){
	$http.get('http://localhost:8080/autor/autores').success(
			function(data){$scope.autores = data;}
	);
}
function bajaAutor($scope, $http, $window) {
    $scope.formBajaAutor = function(idAutor) {
  	  var data = $scope.fields;
	  	$http.get('http://localhost:8080/autor/baja/'+idAutor).success(
			function(response){
		  		  $window.location.href = '../listAutores.html';
			}
		);
    };
	$http.get('http://localhost:8080/autor/autores').success(
			function(data){$scope.autores = data;}
	);
	$scope.autorChanged = function(idAutor){
		getAutor($scope, $http, idAutor)
	};
}
